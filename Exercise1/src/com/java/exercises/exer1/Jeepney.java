/**
 * 
 */
package com.java.exercises.exer1;

/**
 * @author b.orobia
 *
 */
public class Jeepney extends Car {
	
	@Override
	public void horn() {
		System.out.println("Beep! Beep! Beep!");
	}

}
