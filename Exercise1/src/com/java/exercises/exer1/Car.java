/**
 * 
 */
package com.java.exercises.exer1;

/**
 * @author b.orobia
 *
 */
public class Car {
	
	public void horn() {
		System.out.println("My car's horn sounds like: ");
	}

}
